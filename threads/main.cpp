#include <iostream>
#include <fstream>
#include <chrono>
#include <vector>
#include <numeric>
#include <cmath>
#include <thread>

/**
 * Jake Yeagley
 * Tuesday January 10th
 *
 * finding mandelbrot from "Coding the Mandelbrot - C++ Tutorial" on youTube
 * syntax for chrono from gitHub
 */

//create 2D vector to hold the x,y cordinate of the pixal
//*** get 2D vector to work on original project
//*** make multiple programs with differenat number of threads

//hard code the mandlebrot to generate certain sections of the image

int listOfPixals[512][512];

template <typename F>
double timeFunction(F f);
void getMandelbrotPixel(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary);
double mapToReal(int x, int imageWidth, double minReal, double maxReal);
double mapToImaginary(int y, int imageHeight, double minImaginary, double maxImaginary);
int mandelbrotAlgrothim(double cReal, double cImaginary, int max);
double calcAverage(std::vector<double> list);
double calcstandardD(std::vector<double> list);
void call2Threads(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary);
void getMandelbrotPixal2Threads1(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary);
void getMandelbrotPixal2Threads2(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary);
void call4Threads(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary);
void getMandelbrotPixal4Threads1(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary);
void getMandelbrotPixal4Threads2(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary);
void getMandelbrotPixal4Threads3(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary);
void getMandelbrotPixal4Threads4(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary);
void call8Threads(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary);
void getMandelbrotPixal8Threads1(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary);
void getMandelbrotPixal8Threads2(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary);
void getMandelbrotPixal8Threads3(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary);
void getMandelbrotPixal8Threads4(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary);
void getMandelbrotPixal8Threads5(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary);
void getMandelbrotPixal8Threads6(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary);
void getMandelbrotPixal8Threads7(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary);
void getMandelbrotPixal8Threads8(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary);

int main()
{
    //open file
    std::fstream fin("input.txt");
    if(!fin){
        std::cout << "the file didn't open" << std::endl;
        std::cin.ignore();
        return 0;
    }

    int imageWidth, imageHeight, maxN;
    double minReal, maxReal, minImaginary, maxImaginary;

    fin >> imageWidth >> imageHeight >> maxN;
    fin >> minReal >> maxReal >> minImaginary >> maxImaginary;

    //input to get number of threads
    int numThreads;
    do {
    std::cout << "how many threads do you want to test? 1, 2, 4, or 8?" << std::endl;
    std::cin >> numThreads;
    std::cout << std::endl;

    //timer function (time the pixel filling up a vector)
    std::vector<double> listOfTimes;

        //handles number of threads
        if (numThreads == 1) {
            //create first mandelbrot to create the outlier first
            getMandelbrotPixel(imageWidth, imageHeight, maxN, minReal, maxReal, minImaginary, maxImaginary);

            for (int j = 0; j < 10; j++) {
                //timing the getMandelbrotPixel function
                //putting times into a vector
                listOfTimes.push_back(timeFunction([=]() {
                    getMandelbrotPixel(imageWidth, imageHeight, maxN, minReal, maxReal, minImaginary, maxImaginary);
                }));
            }
        } else if (numThreads == 2) {
            //create first mandelbrot to create the outlier first
            call2Threads(imageWidth, imageHeight, maxN, minReal, maxReal, minImaginary, maxImaginary);

            for (int j = 0; j < 10; j++) {
                //timing the getMandelbrotPixel function
                //putting times into a vector
                listOfTimes.push_back(timeFunction([=]() {
                    call2Threads(imageWidth, imageHeight, maxN, minReal, maxReal, minImaginary, maxImaginary);
                }));
            }
        } else if (numThreads == 4) {
            //create first mandelbrot to create the outlier first
            call4Threads(imageWidth, imageHeight, maxN, minReal, maxReal, minImaginary, maxImaginary);

            for (int j = 0; j < 10; j++) {
                //timing the getMandelbrotPixel function
                //putting times into a vector
                listOfTimes.push_back(timeFunction([=]() {
                    call4Threads(imageWidth, imageHeight, maxN, minReal, maxReal, minImaginary, maxImaginary);
                }));
            }
        } else if (numThreads == 8) {
            //create first mandelbrot to create the outlier first
            call8Threads(imageWidth, imageHeight, maxN, minReal, maxReal, minImaginary, maxImaginary);

            for (int j = 0; j < 10; j++) {
                //timing the getMandelbrotPixel function
                //putting times into a vector
                listOfTimes.push_back(timeFunction([=]() {
                    call8Threads(imageWidth, imageHeight, maxN, minReal, maxReal, minImaginary, maxImaginary);
                }));
            }
        }
        else if(numThreads == 0)
                return 0;
        else
            std::cout << "error" << std::endl;


    //write to file
    std::ofstream fout("output.ppm");
    fout << "P3" << std::endl; //"magic number"
    fout << imageWidth << " " << imageHeight << std::endl;
    fout << "255" << std::endl;
    int i = 0;
    for(int y = 0; y < imageHeight; y++) {
        for (int x = 0; x < imageWidth; x++) {
            int n = listOfPixals[x][y];
            i++;
            int r = (n % 256);
            int g = (n % 256);
            int b = (n % 256);

            fout << r << " " << g << " " << b << " ";
        }
    }

    //calculate average and standard Deviation
    std::cout << "the average time is: " << calcAverage(listOfTimes) << std::endl;
    std::cout << "the standard deviation is: " << calcstandardD(listOfTimes) << std::endl;
    }
    while(numThreads != 0);

    return 0;
}
template <typename F>
double timeFunction(F f){
    auto start = std::chrono::steady_clock::now();
    f();
    auto end = std::chrono::steady_clock::now();
    //for debug
    //std::cout << std::chrono::duration <double> (end - start).count() << " s" << std::endl;

    return std::chrono::duration<double> (end - start).count();
}
double mapToReal(int x, int imageWidth, double minReal, double maxReal){
    //algrothim taken from wikipedia and tutorial on youtube (see top)
    double range = maxReal - minReal;
    return x * (range / imageWidth) + minReal;
}
double mapToImaginary(int y, int imageHeight, double minImaginary, double maxImaginary){
    //algrothim taken from wikipedia and tutorial on youtube (see top)
    double range = maxImaginary - minImaginary;
    return y * (range / imageHeight) + minImaginary;
}
void getMandelbrotPixel(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary){

    for(int y = imageHeight; y < 512; y++){
        for(int x = 0; x < imageWidth; x++) {
            //...map to real
            double cReal = mapToReal(x ,imageWidth, minReal, maxReal);
            //...map to imaginary
            double cImaginary = mapToImaginary(y, imageHeight, minImaginary, maxImaginary);
            //...create pixel
            listOfPixals[x][y] = mandelbrotAlgrothim(cReal,cImaginary,maxN);
        }
    }

}
void call2Threads(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary){
    std::thread t1(getMandelbrotPixal2Threads1,imageWidth,imageHeight,maxN,minReal,maxReal,minImaginary,maxImaginary);
    std::thread t2(getMandelbrotPixal2Threads2,imageWidth,imageHeight,maxN,minReal,maxReal,minImaginary,maxImaginary);
    t1.join();
    t2.join();
}
void getMandelbrotPixal2Threads1(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary){
    for(int y = 0; y < 256; y++){
        for(int x = 0; x < 512; x++) {
            //...map to real
            double cReal = mapToReal(x ,imageWidth, minReal, maxReal);
            //...map to imaginary
            double cImaginary = mapToImaginary(y, imageHeight, minImaginary, maxImaginary);
            //...create pixel
            listOfPixals[x][y] = mandelbrotAlgrothim(cReal,cImaginary,maxN);
        }
    }
}
void getMandelbrotPixal2Threads2(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary){
    for(int y = 256; y < 512; y++){
        for(int x = 0; x < 512; x++) {
            //...map to real
            double cReal = mapToReal(x ,imageWidth, minReal, maxReal);
            //...map to imaginary
            double cImaginary = mapToImaginary(y, imageHeight, minImaginary, maxImaginary);
            //...create pixel
            listOfPixals[x][y] = mandelbrotAlgrothim(cReal,cImaginary,maxN);
        }
    }
}
void call4Threads(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary){
    std::thread t1(getMandelbrotPixal4Threads1,imageWidth,imageHeight,maxN,minReal,maxReal,minImaginary,maxImaginary);
    std::thread t2(getMandelbrotPixal4Threads2,imageWidth,imageHeight,maxN,minReal,maxReal,minImaginary,maxImaginary);
    std::thread t3(getMandelbrotPixal4Threads3,imageWidth,imageHeight,maxN,minReal,maxReal,minImaginary,maxImaginary);
    std::thread t4(getMandelbrotPixal4Threads4,imageWidth,imageHeight,maxN,minReal,maxReal,minImaginary,maxImaginary);
    t1.join();
    t2.join();
    t3.join();
    t4.join();
}
void getMandelbrotPixal4Threads1(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary){
    for(int y = 0; y < 256; y++){
        for(int x = 0; x < 256; x++) {
            //...map to real
            double cReal = mapToReal(x ,imageWidth, minReal, maxReal);
            //...map to imaginary
            double cImaginary = mapToImaginary(y, imageHeight, minImaginary, maxImaginary);
            //...create pixel
            listOfPixals[x][y] = mandelbrotAlgrothim(cReal,cImaginary,maxN);
        }
    }
}
void getMandelbrotPixal4Threads2(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary){
    for(int y = 0; y < 256; y++){
        for(int x = 256; x < 512; x++) {
            //...map to real
            double cReal = mapToReal(x ,imageWidth, minReal, maxReal);
            //...map to imaginary
            double cImaginary = mapToImaginary(y, imageHeight, minImaginary, maxImaginary);
            //...create pixel
            listOfPixals[x][y] = mandelbrotAlgrothim(cReal,cImaginary,maxN);
        }
    }
}
void getMandelbrotPixal4Threads3(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary){
    for(int y = 256; y < 512; y++){
        for(int x = 0; x < 256; x++) {
            //...map to real
            double cReal = mapToReal(x ,imageWidth, minReal, maxReal);
            //...map to imaginary
            double cImaginary = mapToImaginary(y, imageHeight, minImaginary, maxImaginary);
            //...create pixel
            listOfPixals[x][y] = mandelbrotAlgrothim(cReal,cImaginary,maxN);
        }
    }
}
void getMandelbrotPixal4Threads4(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary){
    for(int y = 256; y < 512; y++){
        for(int x = 256; x < 512; x++) {
            //...map to real
            double cReal = mapToReal(x ,imageWidth, minReal, maxReal);
            //...map to imaginary
            double cImaginary = mapToImaginary(y, imageHeight, minImaginary, maxImaginary);
            //...create pixel
            listOfPixals[x][y] = mandelbrotAlgrothim(cReal,cImaginary,maxN);
        }
    }
}
void call8Threads(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary){
    std::thread t1(getMandelbrotPixal8Threads1,imageWidth,imageHeight,maxN,minReal,maxReal,minImaginary,maxImaginary);
    std::thread t2(getMandelbrotPixal8Threads2,imageWidth,imageHeight,maxN,minReal,maxReal,minImaginary,maxImaginary);
    std::thread t3(getMandelbrotPixal8Threads3,imageWidth,imageHeight,maxN,minReal,maxReal,minImaginary,maxImaginary);
    std::thread t4(getMandelbrotPixal8Threads4,imageWidth,imageHeight,maxN,minReal,maxReal,minImaginary,maxImaginary);
    std::thread t5(getMandelbrotPixal8Threads5,imageWidth,imageHeight,maxN,minReal,maxReal,minImaginary,maxImaginary);
    std::thread t6(getMandelbrotPixal8Threads6,imageWidth,imageHeight,maxN,minReal,maxReal,minImaginary,maxImaginary);
    std::thread t7(getMandelbrotPixal8Threads7,imageWidth,imageHeight,maxN,minReal,maxReal,minImaginary,maxImaginary);
    std::thread t8(getMandelbrotPixal8Threads8,imageWidth,imageHeight,maxN,minReal,maxReal,minImaginary,maxImaginary);
    t1.join();
    t2.join();
    t3.join();
    t4.join();
    t5.join();
    t6.join();
    t7.join();
    t8.join();
}
void getMandelbrotPixal8Threads1(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary){
    for(int y = 0; y < 128; y++){
        for(int x = 0; x < 128; x++) {
            //...map to real
            double cReal = mapToReal(x ,imageWidth, minReal, maxReal);
            //...map to imaginary
            double cImaginary = mapToImaginary(y, imageHeight, minImaginary, maxImaginary);
            //...create pixel
            listOfPixals[x][y] = mandelbrotAlgrothim(cReal,cImaginary,maxN);
        }
    }
}
void getMandelbrotPixal8Threads2(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary){
    for(int y = 128; y < 256; y++){
        for(int x = 0; x < 128; x++) {
            //...map to real
            double cReal = mapToReal(x ,imageWidth, minReal, maxReal);
            //...map to imaginary
            double cImaginary = mapToImaginary(y, imageHeight, minImaginary, maxImaginary);
            //...create pixel
            listOfPixals[x][y] = mandelbrotAlgrothim(cReal,cImaginary,maxN);
        }
    }
}
void getMandelbrotPixal8Threads3(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary){
    for(int y = 0; y < 128; y++){
        for(int x = 128; x < 256; x++) {
            //...map to real
            double cReal = mapToReal(x ,imageWidth, minReal, maxReal);
            //...map to imaginary
            double cImaginary = mapToImaginary(y, imageHeight, minImaginary, maxImaginary);
            //...create pixel
            listOfPixals[x][y] = mandelbrotAlgrothim(cReal,cImaginary,maxN);
        }
    }
}
void getMandelbrotPixal8Threads4(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary){
    for(int y = 128; y < 256; y++){
        for(int x = 128; x < 256; x++) {
            //...map to real
            double cReal = mapToReal(x ,imageWidth, minReal, maxReal);
            //...map to imaginary
            double cImaginary = mapToImaginary(y, imageHeight, minImaginary, maxImaginary);
            //...create pixel
            listOfPixals[x][y] = mandelbrotAlgrothim(cReal,cImaginary,maxN);
        }
    }
}
void getMandelbrotPixal8Threads5(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary){
    for(int y = 256; y < 512; y++){
        for(int x = 0; x < 256; x++) {
            //...map to real
            double cReal = mapToReal(x ,imageWidth, minReal, maxReal);
            //...map to imaginary
            double cImaginary = mapToImaginary(y, imageHeight, minImaginary, maxImaginary);
            //...create pixel
            listOfPixals[x][y] = mandelbrotAlgrothim(cReal,cImaginary,maxN);
        }
    }
}
void getMandelbrotPixal8Threads6(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary){
    for(int y = 256; y < 512; y++){
        for(int x = 256; x < 512; x++) {
            //...map to real
            double cReal = mapToReal(x ,imageWidth, minReal, maxReal);
            //...map to imaginary
            double cImaginary = mapToImaginary(y, imageHeight, minImaginary, maxImaginary);
            //...create pixel
            listOfPixals[x][y] = mandelbrotAlgrothim(cReal,cImaginary,maxN);
        }
    }
}
void getMandelbrotPixal8Threads7(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary){
    for(int y = 0; y < 512; y++){
        for(int x = 256; x < 384; x++) {
            //...map to real
            double cReal = mapToReal(x ,imageWidth, minReal, maxReal);
            //...map to imaginary
            double cImaginary = mapToImaginary(y, imageHeight, minImaginary, maxImaginary);
            //...create pixel
            listOfPixals[x][y] = mandelbrotAlgrothim(cReal,cImaginary,maxN);
        }
    }
}
void getMandelbrotPixal8Threads8(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary){
    for(int y = 0; y < 512; y++){
        for(int x = 384; x < 512; x++) {
            //...map to real
            double cReal = mapToReal(x ,imageWidth, minReal, maxReal);
            //...map to imaginary
            double cImaginary = mapToImaginary(y, imageHeight, minImaginary, maxImaginary);
            //...create pixel
            listOfPixals[x][y] = mandelbrotAlgrothim(cReal,cImaginary,maxN);
        }
    }
}
int mandelbrotAlgrothim(double cReal, double cImaginary, int max){
    //algrothim taken from wikipedia and tutorial on youtube (see top)
    int i = 0;
    double zReal = 0, zImaginary = 0;
    while(i < max && zReal * zReal + zImaginary * zImaginary < 4.0){
        double temp = zReal * zReal - zImaginary * zImaginary + cReal;
        zImaginary = 2 * zReal * zImaginary + cImaginary;
        zReal = temp;
        i++;
    }

    return i;
}
double calcAverage(std::vector<double> list){
    double average = std::accumulate(list.begin(),list.end(),0.0);
    average = average / list.size();
    return average;
}
double calcstandardD(std::vector<double> list){
    double SD = 0.0;
    double average = calcAverage(list);
    for(int i = 0; i < list.size(); i++){
        SD += pow((list[i] - average),2);
    }

    return sqrt(SD) / list.size();
}