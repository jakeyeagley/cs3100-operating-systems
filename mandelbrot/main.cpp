#include <iostream>
#include <fstream>
#include <chrono>
#include <vector>
#include <numeric>
#include <cmath>

/**
 * Jake Yeagley
 * Tuesday January 10th
 *
 * finding mandelbrot from "Coding the Mandelbrot - C++ Tutorial" on youTube
 * syntax for chrono from gitHub
 */

template <typename F>
double timeFunction(F f);
std::vector<int> getMandelbrotPixel(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary);
double mapToReal(int x, int imageWidth, double minReal, double maxReal);
double mapToImaginary(int y, int imageHeight, double minImaginary, double maxImaginary);
int mandelbrotAlgrothim(double cReal, double cImaginary, int max);
double calcAverage(std::vector<double> list);
double calcstandardD(std::vector<double> list);

int main()
{
    //open file
    std::fstream fin("/Users/jakeyeagley/CLionProjects/mandelbrot/input.txt");
    if(!fin){
        std::cout << "the file didn't open" << std::endl;
        std::cin.ignore();
        return 0;
    }

    int imageWidth, imageHeight, maxN;
    double minReal, maxReal, minImaginary, maxImaginary;

    fin >> imageWidth >> imageHeight >> maxN;
    fin >> minReal >> maxReal >> minImaginary >> maxImaginary;

    //create first mandelbrot to create the outlier first
    getMandelbrotPixel(imageWidth,imageHeight,maxN,minReal,maxReal,minImaginary,maxImaginary);

    //timer function (time the pixel filling up a vector)
    std::vector<double> listOfTimes;
    for(int j = 0; j < 10; j++) {
        //timing the getMandelbrotPixel function
        //putting times into a vector
        listOfTimes.push_back(timeFunction([=]() {
            getMandelbrotPixel(imageWidth, imageHeight, maxN, minReal, maxReal, minImaginary, maxImaginary);
        }));
        //for debug
        //std::cout << listOfTimes.at(j) << std::endl;
    }
    //compute mandelbrot (return a vector of pixels)
    std::vector<int>listOfPixels = getMandelbrotPixel(imageWidth,imageHeight,maxN,minReal,maxReal,minImaginary,maxImaginary);

    //write to file
    std::ofstream fout("output.ppm");
    fout << "P3" << std::endl; //"magic number"
    fout << imageWidth << " " << imageHeight << std::endl;
    fout << "255" << std::endl;
    int i = 0;
    for(int y = 0; y < imageHeight; y++) {
        for (int x = 0; x < imageWidth; x++) {
            int n = listOfPixels.at(i);
            i++;
            int r = (n % 256);
            int g = (n % 256);
            int b = (n % 256);

            fout << r << " " << g << " " << b << " ";
        }
    }
    //calculate average and standard Deviation
    std::cout << "the average time is: " << calcAverage(listOfTimes) << std::endl;
    std::cout << "the standard deviation is: " << calcstandardD(listOfTimes) << std::endl;

    return 0;
}
template <typename F>
double timeFunction(F f){
    auto start = std::chrono::steady_clock::now();
    f();
    auto end = std::chrono::steady_clock::now();
    //for debug
    //std::cout << std::chrono::duration <double> (end - start).count() << " s" << std::endl;

    return std::chrono::duration<double> (end - start).count();
}
double mapToReal(int x, int imageWidth, double minReal, double maxReal){
    //algrothim taken from wikipedia and tutorial on youtube (see top)
    double range = maxReal - minReal;
    return x * (range / imageWidth) + minReal;
}
double mapToImaginary(int y, int imageHeight, double minImaginary, double maxImaginary){
    //algrothim taken from wikipedia and tutorial on youtube (see top)
    double range = maxImaginary - minImaginary;
    return y * (range / imageHeight) + minImaginary;
}
std::vector<int> getMandelbrotPixel(int imageWidth, int imageHeight, int maxN, double minReal, double maxReal, double minImaginary, double maxImaginary){
    std::vector<int> listOfPixels;

    for(int y = 0; y < imageHeight; y++){
        for(int x = 0; x < imageWidth; x++) {
            //...map to real
            double cReal = mapToReal(x ,imageWidth, minReal, maxReal);
            //...map to imaginary
            double cImaginary = mapToImaginary(y, imageHeight, minImaginary, maxImaginary);
            //...create pixel
            listOfPixels.push_back(mandelbrotAlgrothim(cReal,cImaginary,maxN));

        }
    }

    return listOfPixels;
}
int mandelbrotAlgrothim(double cReal, double cImaginary, int max){
    //algrothim taken from wikipedia and tutorial on youtube (see top)
    int i = 0;
    double zReal = 0, zImaginary = 0;
    while(i < max && zReal * zReal + zImaginary * zImaginary < 4.0){
        double temp = zReal * zReal - zImaginary * zImaginary + cReal;
        zImaginary = 2 * zReal * zImaginary + cImaginary;
        zReal = temp;
        i++;
    }

    return i;
}
double calcAverage(std::vector<double> list){
    double average = std::accumulate(list.begin(),list.end(),0.0);
    average = average / list.size();
    return average;
}
double calcstandardD(std::vector<double> list){
    double SD = 0.0;
    double average = calcAverage(list);
    for(int i = 0; i < list.size(); i++){
        SD += pow((list[i] - average),2);
    }

    return sqrt(SD) / list.size();
}