#include <iostream>
#include <vector>
#include <numeric>
#include <cmath>
#include "quickSort.h"
#include "threadedFind.h"

template <typename F>
double timeFunction(F f){
    auto start = std::chrono::steady_clock::now();
    f();
    auto end = std::chrono::steady_clock::now();
    //for debug
    //std::cout << std::chrono::duration <double> (end - start).count() << " s" << std::endl;

    return std::chrono::duration<double> (end - start).count();
}
double calcAverage(std::vector<double> list){
    double average = std::accumulate(list.begin(),list.end(),0.0);
    average = average / list.size();
    return average;
}
double calcstandardD(std::vector<double> list){
    double SD = 0.0;
    double average = calcAverage(list);
    for(int i = 0; i < (int)list.size(); i++){
        SD += pow((list[i] - average),2);
    }

    return sqrt(SD) / list.size();
}

int main() {
    //input for vector size
    int vecSize,threadNum;

    std::cout << "how large of a vector would you like to test? " << std::endl;
    std::cin >> vecSize;
    std::cout << "how many threads would you like to test? " << std::endl;
    std::cin >> threadNum;

    //set size
    std::vector<int> testVector(vecSize);

    //testing
    std::vector<double> stdFindTimes;
    std::vector<double> stdSortTimes;
    std::vector<double> threadSortTimes;
    std::vector<double> threadFindTimes;

    //test 10 times
    srand((unsigned int) time(NULL));
    for(int i = 0; i < 1; i++)
    {
        //initilize
//        std::cout << "initilizing vector" << std::endl;
        for (int j = 0; j < vecSize; j++) {
            testVector[j] = rand() % 51;
//            std::cout << testVector[j] << " ";
        }
//        std::cout << std::endl;

        //find
        //thread find
//        bool found;
        {
            threadedFind<int> finder(threadNum);
            auto start = std::chrono::steady_clock::now();
            //found =
            finder.find(testVector,7);
            auto end = std::chrono::steady_clock::now();
            threadFindTimes.push_back(std::chrono::duration<double> (end - start).count());
//            std::cout << "thread find time number " << i + 1 << ": " << threadFindTimes[i] << std::endl;
        }

//        if(found)
//            std::cout << "the number was found in threaded find " << std::endl;
//        else
//            std::cout << "the number was not found in threaded find" << std::endl;

//        auto result = std::find(testVector.begin(), testVector.end(), 7);
//        if (result != std::end(testVector)) {
//            std::cout << "the number was found in std::find " << std::endl;
//        } else {
//            std::cout << "the number was not found in std::find " << std::endl;
//        }

        //standard find
        stdFindTimes.push_back(timeFunction(
                [=]() { std::find(testVector.begin(), testVector.end(), 7); }));
//        std::cout << "std::find time number " << i + 1 << ": " << stdFindTimes[i] << std::endl;


        //sort
        //thread sort
        {
            quickSort<int> sort(threadNum);
            auto start = std::chrono::steady_clock::now();
            sort.run(testVector);
            auto end = std::chrono::steady_clock::now();
            threadSortTimes.push_back(std::chrono::duration<double> (end - start).count());
//            std::cout << "thread sort time number " << i + 1 << ": " << threadSortTimes[i] << std::endl;
        }

//        for (auto a : testVector) {
//            std::cout << a << " ";
//        }

        //standard sort
        stdSortTimes.push_back(timeFunction([&]() { std::sort(testVector.begin(), testVector.end()); }));
//        std::cout << "std::sort time number " << i + 1 << ": " << stdSortTimes[i] << std::endl;

//        for (auto a : testVector) {
//            std::cout << a << " ";
//        }
    }
    std::cout << "--------- times ----------" << std::endl;
    std::cout << "the average time for std::find is " << calcAverage(stdFindTimes) << std::endl;
//    std::cout << "the standard deviation is " << calcstandardD(stdFindTimes) << std::endl << std::endl;
    std::cout << "the average time for thread find is " << calcAverage(threadFindTimes) << std::endl;
//    std::cout << "the standard deviation is " << calcstandardD(threadFindTimes) << std::endl  << std::endl;
    std::cout << "the average time for std::sort is " << calcAverage(stdSortTimes) << std::endl;
//    std::cout << "the standard deviation is " << calcstandardD(stdSortTimes) << std::endl << std::endl;
    std::cout << "the average time for thread sort is " << calcAverage(threadSortTimes) << std::endl;
//    std::cout << "the standard deviation is " << calcstandardD(threadSortTimes) << std::endl;


    return 0;
}