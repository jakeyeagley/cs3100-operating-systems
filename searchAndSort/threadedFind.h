//
// Created by Jake Yeagley on 2/8/17.
//

#ifndef SEARCHANDSORT_THREADEDFIND_H
#define SEARCHANDSORT_THREADEDFIND_H


#include <condition_variable>
#include <vector>
#include <thread>
#include <algorithm>
#include <atomic>
#include <mutex>
#include <queue>
#include <thread>
#include <iostream>

template <typename T>
class threadedFind {
private:
    std::vector<std::thread> pool;
    std::queue<std::vector<T>> tasks;
    std::vector<int> points;
    std::mutex qMutex,sMutex;
    std::condition_variable cv;
    std::atomic<bool> shouldContinue, found;
    int threadCount, num, partition;
    void stop() {
        shouldContinue = false;
        cv.notify_all();
    }
//    void print(std::vector<T> &vector)
//    {
//        for (int i = 0; i < vector.size(); i++)
//            std::cout << vector[i] << " ";
//        std::cout << std::endl;
//    }
    void run(){
//        std::cout << "run was called" << std::endl;
        while(shouldContinue){
            std::unique_lock<std::mutex> lock(qMutex);
            if(found) {
                stop();
                found = true;
                return;
            }
            if(tasks.size() == 0){
                stop();
                found = false;
                return;
            }
//            std::cout << "tasks size is: " << tasks.size() << std::endl;
            if(tasks.size() > 0 && !found){
                std::vector<T> currentTask = tasks.front();
                tasks.pop();

                sMutex.lock();
//                std::cout << "the current task is: " << std::endl;
//                print(currentTask);
                auto result = std::find(currentTask.begin(),currentTask.end(),num);
                sMutex.unlock();
                if(result != std::end(currentTask)) {
                    stop();
                    found = true;
                    return;
                }

                cv.notify_one();
            }
            else {
                cv.wait(lock);
            }
        }
    }
    void setTasks(std::vector<T> vector){
//        int size = (int)vector.size();
            for(int i = 0; i < threadCount; i++){
                typename std::vector<T>::const_iterator first = vector.begin() + points[i];
                typename std::vector<T>::const_iterator second = vector.begin() + points[i + 1];
                typename std::vector<T> tempVec(first,second);
                tasks.push(tempVec);
            }
    }
    void setPoints(std::vector<T> vector){
        //get points starting at 1 leaving point 0 at 0
        for(int i = 1; i < threadCount; i++) {
            points[i] = partition * i;
//            std::cout << "inside for loop" << std::endl;
        }
        //account for integer division
        if(vector.size() % 2 != 0)
            vector.push_back(0);

        points.push_back((int)vector.size());
    }

public:
    threadedFind(unsigned long threads):pool(threads),points(threads),shouldContinue(true),found(false){
        threadCount = (int)threads;
    };
    bool find(std::vector<T> &vector,int num_){
//        std::cout << "find was called" << std::endl;
        num = num_;
        partition = (int)vector.size()/threadCount;

        setPoints(vector);
        setTasks(vector);
//        std::cout << "calling threads" << std::endl;
        for(auto&& t:pool) {
            t = std::thread([&]() { this->run(); });
        }

        return found;
    }
    ~threadedFind(){
        for(auto&& t:pool)
            t.join();
    }
};


#endif //SEARCHANDSORT_THREADEDFIND_H
