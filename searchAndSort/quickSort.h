//
// Created by Jake Yeagley on 2/7/17.
//

#ifndef SEARCHANDSORT_QUICK_SORT_H
#define SEARCHANDSORT_QUICK_SORT_H

#include <vector>
#include <queue>
#include <mutex>
#include <set>
#include <thread>
#include <iostream>
#include <atomic>
#include <condition_variable>
#include <iostream>


//parts taken from stakeoverflow.com
template <typename T>
class quickSort {
private:
    std::vector<std::thread> pool;
    std::atomic<bool> shouldContinue;
    std::queue< std::pair<int, int> > tasks;
    std::mutex qMutex,sMutex;
    std::condition_variable cv;
    std::set<int> set;
    void stop() {shouldContinue = false; cv.notify_all();}
    //for debugging
//    void print(std::vector<T> &vector)
//    {
//        for (int i = 0; i < (int)vector.size(); i++)
//            std::cout << vector[i] << " ";
//        std::cout << std::endl;
//    }
    bool isSorted(std::vector<T> &vector)
    {
        for (int i = 0; i < (int)vector.size(); i++)
            if (!(vector[i] <= vector[i + 1]))
                return false;
        return true;
    }
    int partition(std::vector<T> &vector, int left, int right){
        T pivot = vector[left];
        while(left != right){
            if(vector[left] > vector[right])
                std::swap(vector[left],vector[right]);
            if(pivot == vector[left])
                right--;
            else
                left++;
        }
        return left;
    }
    //parts taken from stakeoverflow.com
    void sort(std::vector<T> &vector){
        while(shouldContinue) {
            std::unique_lock<std::mutex> qLock(qMutex);
//            print(vector);
//            std::cout << "is sorted: " << isSorted(vector) << std::endl;
            if (set.size() == (unsigned int)vector.size()) {
                stop();
                return;
            }
            else if(isSorted(vector)){
                stop();
                return;
            }
            if (tasks.size() > 0) {
                std::pair<int, int> currentTask = (std::pair<int, int> &&) tasks.front();
                tasks.pop();

                int left = currentTask.first, right = currentTask.second;

                if (left < right) {
                    int part = partition(vector, left, right);

                    sMutex.lock();
                    set.insert(part);
                    set.insert(left);
                    set.insert(right);
                    sMutex.unlock();

                    std::pair<int, int> toBePushed;
                    toBePushed.first = left;
                    toBePushed.second = part - 1;
                    tasks.push(toBePushed);
                    toBePushed.first = part + 1;
                    toBePushed.second = right;
                    tasks.push(toBePushed);

                    cv.notify_one();
                }
            }
            else
                cv.wait(qLock);

        }
    }

public:
    quickSort(unsigned long threadCount):pool(threadCount),shouldContinue(true){}
    ~quickSort(){
        for(auto&&t:pool)
            t.join();
    }
    void run(std::vector<T> vector){
//        std::cout << "run was called" << std::endl;
        std::pair<int,int> toBePushed;
        toBePushed.first = 0;
        toBePushed.second = ((int)vector.size()) - 1;
        tasks.push(toBePushed);

        for(auto&& t:pool)
            t = std::thread([&](){ this->sort(vector); });
    }

};


#endif //SEARCHANDSORT_QUICK_SORT_H
