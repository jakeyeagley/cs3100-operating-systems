#include <iostream>
#include <unistd.h>
#include <vector>
#include <sstream>
#include <chrono>
#include <string.h>
#include <algorithm>
#include <sys/wait.h>

//parts taken from stackoverflow.com
char *convert(const std::string & s)
{
    char *pc = new char[s.size()+1];
    strcpy(pc, s.c_str());
    return pc;
}
//parts taken from class
std::vector<char*> getCommand()
{
    std::string line;
    std::vector<std::string> words;

    getline(std::cin,line);
    auto cur = line.begin();
    while(cur != line.end())
    {
        auto next = std::find(cur,line.end(),' ');
        words.emplace_back(cur,next);
        cur = next;

        if(cur != line.end())
            ++cur;
    }

    std::vector<char*> args;
    std::transform(words.begin(),words.end(),std::back_inserter(args),convert);
    args.push_back(nullptr);

    return args;
};
//parts taken from class
int main() {
    std::vector<char*> args;
    std::vector<char*> history;
    double time = 0.0;

    while(true) {
        std::cout << "[cmd]: ";
        args = getCommand();

        if(strcmp(args[0],"^") == 0)
        {
            char a = *args[1];
            //starting at a index of 1
            int num = a - 49;
            if(num < (int)history.size() && num >= 0)
                std::cout << history[num] << std::endl;
            else
                std::cout << "out of range" << std::endl;

            continue;
        }

        history.push_back(args[0]);

        if(strcmp(args[0],"exit") == 0)
            _exit(0);

        if(strcmp(args[0],"history") == 0)
        {
            for(int i = 0; i < (int)history.size() - 1; i++)
                std::cout << i + 1 << ": " << history[i] << std::endl;
            continue;
        }

        if(strcmp(args[0],"ptime") == 0) {
            std::cout << "Time spent executing child processes: " << time << " microseconds" << std::endl;
            continue;
        }

        auto start = std::chrono::steady_clock::now();
        auto pid = fork();                      //fork spawns the child
        if (pid < 0) {
            //error with forking
            perror("fork failed \n");
            exit(5);
        }
        if (pid == 0) {
            //do childs stuff (call exec)
            execvp(args[0], args.data());       //nothing after exec will be exectued if exec ran correctly
            exit(1);
        }
        if(pid > 0){
            //parent stuff (wait)
            int status;
            waitpid(pid, &status, 0);
        }
        auto end = std::chrono::steady_clock::now();
        time += std::chrono::duration_cast<std::chrono::microseconds> (end - start).count();
    };

}