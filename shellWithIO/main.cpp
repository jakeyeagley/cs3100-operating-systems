#include <iostream>
#include <zconf.h>
#include <vector>
#include <fstream>
#include <chrono>
#include <fcntl.h>
#include <unistd.h>
#include <sstream>
#include <string.h>
#include <algorithm>
#include <sys/wait.h>

char *convert(const std::string & s)
{
    char *pc = new char[s.size()+1];
    strcpy(pc, s.c_str());
    return pc;
}
std::vector<char*> getCommand(std::string line)
{
    std::vector<std::string> words;
    auto cur = line.begin();
    while(cur != line.end())
    {
        auto next = std::find(cur,line.end(),' ');
        words.emplace_back(cur,next);
        cur = next;

        if(cur != line.end())
            ++cur;
    }

    std::vector<char*> args;
    std::transform(words.begin(),words.end(),std::back_inserter(args),convert);
    args.push_back(nullptr);

    return args;
};

int main()
{

    std::cout << "**disclaimer to grader**" << std::endl;
    std::cout << "Not all functions work" << std::endl;
    std::cout << "functions that I've tested and ran successfully" << std::endl;
    std::cout << "exe < input.txt : for example cat < input.txt " << std::endl;
    std::cout << "exe > output.txt : for example cal > output.txt " << std::endl;
    std::cout << "exe (parameters) | exe : for example ls -l | sort" << std::endl;
    std::cout << "all previous functions to last assignment : for example ls, date, ptime, history, and exit" << std::endl;

    std::vector<char*> args;
    std::vector<char*> history;
    double time = 0.0;

    int p[2];
    const int WRITE_END = 1;
    const int READ_END = 0;
    pipe(p);

    while(true) {
        std::cout << "[cmd]: ";
        std::string line;
        getline(std::cin,line);
        args = getCommand(line);

        if(strcmp(args[0],"^") == 0)
        {
            char a = *args[1];
            //starting at a index of 1
            int num = a - 49;
            if(num < (int)history.size() && num >= 0)
                std::cout << history[num] << std::endl;
            else
                std::cout << "out of range" << std::endl;

            continue;
        }

        if(strcmp(args[0],"exit") == 0)
            _exit(0);

        if(strcmp(args[0],"history") == 0)
        {
            history.push_back(args[0]);
            for(int i = 0; i < (int)history.size() - 1; i++)
                std::cout << i + 1 << ": " << history[i] << std::endl;
            continue;
        }

        if(strcmp(args[0],"ptime") == 0) {
            history.push_back(args[0]);
            std::cout << "Time spent executing child processes: " << time << " microseconds" << std::endl;
            continue;
        }

        //parts taken from CS tutor
        auto start = std::chrono::steady_clock::now();
        if((int)line.find("|") != -1) {
            history.push_back(args[0]);
            char **newArgs = new char *[3];
            newArgs[0] = new char[strlen(args[0])];
            strcpy(newArgs[0], args[0]);
            newArgs[1] = new char[strlen(args[0])];
            strcpy(newArgs[1], args[1]);
            newArgs[2] = nullptr;

            auto pid = fork();
            if (pid == 0) {
                dup2(p[WRITE_END], STDOUT_FILENO);
                execvp(newArgs[0], newArgs);
            }

            auto pid2 = fork();
            if (pid2 == 0) {
                newArgs = new char *[2];
                newArgs[0] = new char[strlen(args[3])];
                strcpy(newArgs[0], args[3]);
                newArgs[1] = nullptr;
                dup2(p[READ_END], STDIN_FILENO);
                close(p[WRITE_END]);
                execvp(newArgs[0], newArgs);
            }
            close(p[WRITE_END]);
            close(p[READ_END]);
            int status = 0;
            waitpid(pid, &status, 0);
            dup2(dup(STDOUT_FILENO), STDOUT_FILENO);
            dup2(dup(STDIN_FILENO), STDIN_FILENO);
        }

        //input
        //parts taken from stakeoverflow.com
        else if((int)line.find("<") != -1)
        {
            history.push_back(args[0]);
            char** newArgs = new char*[2];
            newArgs[0] = new char[strlen(args[0])];
            strcpy(newArgs[0],args[0]);
            newArgs[1] = nullptr;
            int inputFileD = open(args[2], O_RDONLY,O_APPEND);

            auto pid = fork();
            if(pid < 0)
            {
                perror("fork failed \n");
                exit(5);
            }
            if(pid == 0)
            {
                dup2(inputFileD, STDIN_FILENO);
                close(inputFileD);
                execlp(newArgs[0], newArgs[0], nullptr);
                exit(1);
            }
            if(pid > 0)
            {
                int status;
                waitpid(pid, &status, 0);
            }
        }

        //output
        //parts taken from stackoverflow.com
        else if((int)line.find(">") != -1)
        {
            history.push_back(args[0]);
            char** newArgs = new char*[2];
            newArgs[0] = new char[strlen(args[0])];
            strcpy(newArgs[0],args[0]);
            newArgs[1] = nullptr;
            int outputFileD = open(args[2], O_WRONLY|O_CREAT);

            auto pid = fork();

            if(pid < 0)
            {
                perror("fork failed here\n");
                exit(5);
            }
            if(pid == 0)
            {
                dup2(outputFileD, STDOUT_FILENO);
                close(outputFileD);
                execlp(newArgs[0], newArgs[0], nullptr);
                exit(1);
            }
            if(pid < 0)
            {
                int status;
                waitpid(pid, &status, 0);
            }
        }
        else
        {
            history.push_back(args[0]);
            auto pid = fork();                      //fork spawns the child
            if (pid < 0) {
                //error with forking
                perror("fork failed \n");
                exit(5);
            }
            if (pid == 0) {
                //do childs stuff (call exec)
                execvp(args[0], args.data());       //nothing after exec will be exectued if exec ran correctly
                exit(1);
            }
            if (pid > 0) {
                //parent stuff (wait)
                int status;
                waitpid(pid, &status, 0);
            }
        }
        auto end = std::chrono::steady_clock::now();
        time += std::chrono::duration_cast<std::chrono::microseconds> (end - start).count();
    };
}